
var AdaptiveAlgorithmInterface = oopi.interface({
	MAX_GEN:'number',
	POP_SIZE:'number',
	score: 'function'
});

var AdaptiveAlgorithm = oopi.abstract({
	DELAY:10,
	INDI:{str:'',score:5000},
	X1:null,
	X2:null,
	Xr:null,
	currentGeneration:null,
	construct: function () {
		this.X1 = [];
		this.X2 = [];
		this.Xr = [];
		for(var i = 0;i < this.POP_SIZE;i++)
			this.X1[i] = this.clone(this.INDI);
	},
	start: function (cb) {
		var self = this;
		var next = function () {
			self.currentGeneration++;
			self.generation();
			if(typeof self.report === 'function') self.report();
			if(self.X1[0].score === 0)
				cb(self.X1[0].str);
			else if(self.currentGeneration <= self.MAX_GEN)
				setTimeout(next,self.DELAY);
			else
				throw new Error('Could not complete adaptation');
		};
		this.currentGeneration = 0;
		next();
	},
	generation: function (cb) {
		for(var i = 0;i < this.POP_SIZE;i++) {
			this.X2[i] = this.clone(this.X1[i]);
			this.Xr[i] = this.clone(this.X2[i]);
			this.mutate(this.Xr[i]);
			this.score(this.Xr[i]);
		}

		this.sort();
	},
	firstNotNull: function () {
		for(var i = 0;i < this.POP_SIZE*2;i++)
			if(this.getAt(i))
				return i;

		return null;
	},
	getAt: function (i) {
		if(i < this.POP_SIZE) return this.X2[i];
		else if(i < this.POP_SIZE*2) return this.Xr[i%this.POP_SIZE];
	},
	sort: function () {
		var best,c;
		for(var j = 0;j < this.POP_SIZE;j++) {
			best = this.firstNotNull();
			for(var i = best;i < this.POP_SIZE*2;i++) {
				if(this.getAt(i) !== null && this.getAt(i).score < this.getAt(best).score)
					best = i;
			}
			this.X1[j] = this.getAt(best);
			var c = this.getAt(best);
			c = null;
		}
	},
	mutate: function (indi) {
		var len = indi.str.length;
		var num = rand.num(3*(len+1)+1);
		if(num <= 1*(len+1)+1) {
			//add
			indi.str = indi.str.substr(0,num%(len+1))+rand.char()+indi.str.substr(num%(len+1));
		} else if(num <= 2*(len+1)) {
			//remove
			indi.str = indi.str.substr(0,num%len)+indi.str.substr(num%len+1);
		} else if(num < 3*(len+1)) {
			//edit
			indi.str = indi.str.substr(0,num%len)+rand.char()+indi.str.substr(num%len+1);
		}
	},
	clone: function (obj) {
		return JSON.parse(JSON.stringify(obj));
	}
}).implements(AdaptiveAlgorithmInterface);

var StringMatch = AdaptiveAlgorithm.prototype.extend({
	MAX_GEN:100,
	POP_SIZE:100,
	target: null,
	best: null,
	construct: function (target) {
		this.target = target;
		this.best = this.X1[0].score;
	},
	report: function () {
		if(this.X1[0].score !== this.best) {
			this.best = this.X1[0].score;
			console.log(this.currentGeneration,this.X1[0].score,this.X1[0].str);
		}
	},
	score: function (indi) {
		indi.score = this.target.length;
		indi.score += Math.abs(this.target.length-indi.str.length);
		for(var i = 0;i < indi.str.length && i < this.target.length;i++)
			if(indi.str.charAt(i) === this.target.charAt(i))
				indi.score--;
	}
});

var rand = {
	chars:'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ,.-_+=\'"',
	num: function (max) {
		return Math.random()*max;
	},
	int: function (max) {
		return Math.floor(this.num(max));
	},
	char: function () {
		return this.chars.charAt(this.num(this.chars.length));
	},
	str: function (len) {
		var str = '';
		for(var i = 0;i < len;i++)
			str += this.char();
		return str;
	}
};