/*
oopi js v0.1.1 (alpha)
Copyright (c) 2014, Stipulus Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/
var oopi = {};
(function () {
    var pub = oopi;
    var rand = {
        len: 6,
        chars: 'abacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',
        gen: function () {
            var str = '';
            var charlen = this.chars.length;
            for(var i = this.len;i > 0;i--)
                str += this.chars.charAt(Math.floor(Math.random()*charlen));
            return str;
        }
    };
    function newobj(obj) {
        if(obj === 'object') {
            var newobj = {};
            for(var i in obj)
                newobj[i] = newobj(obj[i]);
            return newobj;
        } else {
            return obj;
        }
    }
    var base = {
        extend: function (child,abstract) {
            child.__constructors = new Array();
            if(this.__constructors) 
                child.__constructors = child.__constructors.concat(this.__constructors);
                
            if(typeof child.construct === 'function') {
                child.__constructors.unshift(child.construct);
            }
            for(var i in this)
                if(i === '__constructors');//do nothing
                else if(typeof child[i] === 'undefined' && i !== 'super') {
                    child[i] = newobj(this[i]);
                } else {
                    if(typeof child.super === 'undefined') {
                        child.super = {};
                    }
                    if(typeof this[i] === 'function') {
                        if(i !== '__constructors')
                            child.super[i] = this[i].bind(child);
                    } else {
                        child.super[i] = newobj(this[i]);
                    }
                }
            if(!abstract) {
                validateInterface(child);
            }
            function F() {
                if(abstract)
                    throw new Error('Cannot construct abstract class.');
                for(var i = child.__constructors.length-1;i >= 0;i--)
                    child.__constructors[i].apply(this,arguments);
            }
            F.implements = function (interface) {
                if(typeof child.__interface === 'object')
                    for(var i in interface)
                        child.__interface[i] = interface[i];
                else
                    child.__interface = interface;
                if(!abstract) validateInterface(child);
                return F;
            }
            F.prototype = child;
            return F;
        }
    };
    function validateInterface(child) {
        if(typeof child.__interface === 'object')
            for(var i in child.__interface)
                if(typeof child[i] !== child.__interface[i])
                    throw new Error('Required attribute or method '+i+' is '+(typeof child[i])+', expected '+child.__interface[i]+'.');
    }
    pub.abstract = function (child) {
        return base.extend(child,true);
    };
    pub.class = function (child) {
        return base.extend(child);
    };
    pub.implement = function (interface,child) {
        if(typeof child.__interface === 'object')
            for(var i in interface)
                child.__interface[i] = interface[i];
        else
            child.__interface = interface;
        validateInterface(child);
        return pub.class(child);
    };
    pub.interface = function (child) {
        for(var i in child)
            switch (child[i]) {
                case 'undefined':
                case 'object':
                case 'boolean':
                case 'number':
                case 'string':
                case 'symbol':
                case 'function':
                case 'object':
                break;
                default:
                throw new Error('Interface definition expected a datatype for '+i+', found '+child[i]+'.');
            }
        return child;
    };
})();
if(typeof document === 'undefined')
    for(var i in oopi)
        exports[i] = oopi[i];