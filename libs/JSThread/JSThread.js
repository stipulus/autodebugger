/*
JSThread v0.1.0 (alpha)
Copyright (c) 2014, Tyler W. Chase-Nason
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/
var JSThread = (function () {
    if (typeof oopi === 'undefined') {
        var oopi={};if(function(){function t(t){if("object"===t){var e={};for(var r in t)e[r]=e(t[r]);return e}return t}function e(t){if("object"==typeof t.__interface)for(var e in t.__interface)if(typeof t[e]!==t.__interface[e])throw new Error("Required attribute or method "+e+" is "+typeof t[e]+", expected "+t.__interface[e]+".")}var r=oopi,n={extend:function(r,n){function o(){if(n)throw new Error("Cannot construct abstract class.");for(var t=r.__constructors.length-1;t>=0;t--)r.__constructors[t].apply(this,arguments)}r.__constructors=new Array,this.__constructors&&(r.__constructors=r.__constructors.concat(this.__constructors)),"function"==typeof r.construct&&r.__constructors.unshift(r.construct);for(var c in this)"__constructors"===c||("undefined"==typeof r[c]&&"super"!==c?r[c]=t(this[c]):("undefined"==typeof r["super"]&&(r["super"]={}),"function"==typeof this[c]?"__constructors"!==c&&(r["super"][c]=this[c].bind(r)):r["super"][c]=t(this[c])));return n||e(r),o["implements"]=function(t){if("object"==typeof r.__interface)for(var c in t)r.__interface[c]=t[c];else r.__interface=t;return n||e(r),o},o.prototype=r,o}};r["abstract"]=function(t){return n.extend(t,!0)},r["class"]=function(t){return n.extend(t)},r.implement=function(t,n){if("object"==typeof n.__interface)for(var o in t)n.__interface[o]=t[o];else n.__interface=t;return e(n),r["class"](n)},r["interface"]=function(t){for(var e in t)switch(t[e]){case"undefined":case"object":case"boolean":case"number":case"string":case"symbol":case"function":case"object":break;default:throw new Error("Interface definition expected a datatype for "+e+", found "+t[e]+".")}return t}}(),"undefined"==typeof document)for(var i in oopi)exports[i]=oopi[i];
    }
    //private
    var Thread = oopi.class({
        id:0,
        blob: null,
        worker: null,
        responseName: 'res',
        data: '{}',
        include: [],
        inprogress: false,
        autoDestroy: false,
        autoStart: false,
        isBusy: false,
        onmessage: function (e) {console.log(e);},
        construct: function (params) {
            if(typeof params !== 'object') params = {};
            this.addIncludes(params.include);
            this.setData(params.data);
            this.setRun(params.run);
            this.setCallback(params.callback);
            this.setAutoDestroy(params.autoDestroy);
            this.setAutoStart(params.autoStart);
            if(this.blob === null)
                this.setBlob(this.buildFunStr());
            if(this.autoStart) this.start();
        },
        setData: function (v) {
            if(typeof v !== 'undefined')
                this.data = JSON.stringify(v, function (key, val) {
                    if (typeof val === 'function') {
                        return val + ''; // implicitly `toString` it
                    }
                    return val;
                });
        },
        setAutoStart: function (v) {
            if(typeof v !== 'undefined')
                this.autoStart = v;
        },
        setAutoDestroy: function (v) {
            if(typeof v !== 'undefined')
                this.autoDestroy = v;
        },
        addIncludes:function (str) {
            if(typeof str !== 'undefined' && str instanceof Array) {
                for(var i = 0;i < str.length;i++)
                    this.addIncludes(str[i]);
            } else if(typeof str === 'string') {
                this.include.push(str);
            }
        },
        getBaseURL: function () {
            var url = document.location.href;
            var index = url.lastIndexOf('/');
            if (index != -1) {
                url = url.substring(0, index+1);
            }
            return url;
        },
        setCallback: function (v) {
            if(typeof v !== 'undefined')
                this.callback = v;
        },
        removeInclude: function (str) {
            var found = false;
            for(var i = 0;i < this.include.length;i++)
                if(this.include[i] === str)  {
                    this.include[i].unshif();
                    found = true;
                    break;
                }
            return found;
        },
        getIncludes: function () {
            
            if(this.include.length > 0) {
                var url = this.getBaseURL();
                return 'importScripts(\''+url+this.include.join('\',\''+url+'')+'\');';
            } else
                return 'importScripts();';
        },
        setBlob: function (str) {
            if(typeof str !== 'undefined') {
                try {
                    this.blob = window.URL.createObjectURL(new Blob([str],{type:"text/javascript"}));
                } catch (e) {
                    this.blob = window.webkitURL.createObjectURL(new Blob([str],{type:"text/javascript"}));
                }
            }
        },
        setRun: function (v) {
            if(typeof v != 'undefined') {
                this.run = v;
                this.setBlob(this.buildFunStr());
            }
        },
        setResponseName: function (v) {
            if(typeof v === 'string' && v.length > 0)
                this.responseName = v;
        },
        funToCodeStr: function (v) {
            var funstr = v.toString();
            var start = funstr.indexOf('{')+1;
            return funstr.substr(start,funstr.lastIndexOf('}')-start);
        },
        buildFunStr: function () {
            if(typeof this.run !== 'undefined') {
                var funstr = this.run.toString();
                this.setResponseName(funstr.substr(funstr.indexOf('(')+1,funstr.indexOf(')')-funstr.indexOf('(')-1));
                var start = funstr.indexOf('{')+1;
                //var prefix = 'var '+this.responseName+' = eval('+this.data+');'+this.getIncludes();
                var prefix = 'var exports = {};'+this.getIncludes()+'onmessage = function (e) {eval(e.data);};'+'var '+this.responseName+' = eval('+this.data+');';
                var suffix = 'postMessage("complete "+JSON.stringify('+this.responseName+',function (key,val) {if (typeof val === "function") return val+"";else if(typeof val === "string") val = encodeURIComponent(val);return val;}));';
                funstr = prefix+funstr.substr(start,funstr.lastIndexOf('}')-start)+suffix;
                start=null,prefix=null,suffix=null;
                return funstr;
            }
        },
        parseMessage: function (e) {
            var arr = new Array();
            if(typeof e.data === 'string')
                arr = e.data.split(' ',2);
            switch (arr[0]) {
                case 'complete': 
                    this.setResponse(arr[1]);
                    this.callCallback();
                    this.inprogress = false;
                    if(this.autoDestroy) this.destroy();
                    break;
                default: this.onmessage(e.data);break;
            }
        },
        setResponse: function (str) {
            if(typeof str !== 'undefined')
                this.response = JSON.parse(str, function (key,val) {
                    if(typeof val === 'string') val = decodeURIComponent(val);
                    return val;
                });
        },
        getResponse: function () {
            if(typeof this.response === 'undefined')
                return {};
            else
                return this.response;
        },
        callCallback: function () {
            if(typeof this.callback !== 'undefined') 
                this.callback(this.getResponse());
        },
        destroy: function () {
            this.worker.terminate();
            window.URL.revokeObjectURL(this.blob);
            delete this.blob,this.worker,this.callback,this.include;
        },
        start: function () {
            this.inprogress = true;
            this.worker = new Worker(this.blob);
            var self = this;
            this.worker.onmessage = function (e) {
                self.parseMessage(e);
            };
        },
        addTask: function (fun) {
            this.worker.postMessage(this.funToCodeStr(fun));
        },
        busy: function () {
            this.isBusy = true;
        },
        ready: function () {
            this.isBusy = false;
        }
    });
    var rand = {
        len: 6,
        chars: 'abacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',
        gen: function () {
            var str = '';
            var charlen = this.chars.length;
            for(var i = this.len;i > 0;i--)
                str += this.chars.charAt(Math.floor(Math.random()*charlen));
            return str;
        }
    };
    return Thread;
})();